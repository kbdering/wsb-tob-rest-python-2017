import random
import re
from TrelloAuth import *
from bs4 import BeautifulSoup
from TestReport import *

report_columns = ["Operation", "Endpoint", "Status", "Object Type",
                  "Passed Parameters", "Request", "Response Code", "Response Text"
                    , "Operated object", "Missed values"]

test_results = []

# dictionary of objects used for the testing
# object identifiers are inconsistent and have to be hard-coded
# format : {identifier : object_type}
id_dict = {
"idModel": "", # idModel represents any ID of any object
"idList" : "lists",
"idBoard" : "boards",
"idCard" : "cards",
"idChecklist" : "checklists",
"idCheckItem" : "checkItems",
"idAttachment": "attachments",
"idLabel" : "labels",
"idWebhook": "webhooks",
"idMember" : "members"
}
path_ids = {
"board_id" : "boards",
"card id or shortlink" : "cards",
"idOrg or name": "organizations",
"idChecklist" : "checklists",
"idLabel" : "labels",
"idWebhook" : "webhooks",
"idCheckItem" : "checkItems",
"idMember": "members",
"idList": "lists"
}

obj_keys_dict = {}#reversed id_dict stored in memory for easier access:
for key, value in id_dict.items():
    obj_keys_dict[value] = key
url_base = "https://api.trello.com"
#initiating the list of active objects
active_objects = []
def find_object_by(obj_id = None, obj_type= None):
    "Finding active object either by object ID or type"
    try:
        matched_objects = [obj for obj in active_objects if obj.obj_id == obj_id or obj.obj_type == obj_type]
        if matched_objects:
            return matched_objects[random.randint(0, len(matched_objects)-1)]
        elif obj_type:
                return create_object(obj_type)
        else:
            return None
    except KeyError:
        return None
def remove_object(obj):
    """removing parent and children objects from active objects list"""
    active_objects.remove(obj)

    children_list = [children for children in active_objects if children.parent_obj_id == obj.obj_id]
    """deleting all the children recursively. Empty list returns False if blank"""
    while children_list:
        parents_list = children_list
        parents_ids = [parent.obj_id for parent in parents_list]

        for parent in parents_list:
            active_objects.remove(parent)
        children_list = [obj for obj in active_objects if obj.parent_obj_id in parents_ids]

def create_object(obj_type):
    try:
        obj = CreateRequest.req_list.get(obj_type).create_object()
        return obj
    except KeyError:
        print( "type " + obj_type + " not found")
        return None
def get_trello_docs():
    list_items = []
    url_base = 'https://developers.trello.com/'
    req = requests.get( url_base + 'templates/apis/advanced-reference.html')
    #print(req.content)
    """ parse data from the webpage to extract all Trello API objects """
    soup = BeautifulSoup(req.content, 'html.parser')
##    print(soup.prettify(formatter="html"))
    for li in soup.div.ul:
        try:
            list_items.append(li.a.text)
            print (li.a.text)
        except ( AttributeError , KeyError):
            pass
##    print (soup.div.ul)
    object_documentation_link = lambda x : url_base + "templates/docs/{0}.html".format(x)
    for item in list_items:
        form_trello_object(object_documentation_link(item))
all_arguments = []
def form_trello_object(link):
    """Not applicable yet. The method is pending on documentation's unification"""
    r = requests.get(link)
    soup = BeautifulSoup(r.text, 'html.parser')
# parsing information for each available endpoint
    for div in soup:
        try:
##            print (div.h2.text)
            request_head = div.h2.text.split(" ")
            request_type = request_head[0]
            request_address = " ".join(request_head[1:])
            permission  = div.ul.li if div.ul.li.text.startswith("Required permissions:") else None
            #extracting arguments
            if permission:
                try:
                    arguments = div.ul.find_all("li")[1]
                except IndexError as e:
                    arguments = div.ul.find_all("li")[0]
            else:
                arguments = div.ul.find_all("li")[0]
#          checking if endpoint requires any arguments
            if arguments.text == "Arguments: None":
                arguments = None
            else:
                print ("arguments found:")
                print (arguments.text)
                i = 0 
                try:
                    for li in  arguments.ul:
                        if len(li) > 1:
                            i +=1
                            j = 0
                            params_list = [element for element in li]
                            field_name = params_list[0]
                            required = params_list[1]
                            try:
                                valid_values = params_list[2]
                            except IndexError:
                                pass
                            all_arguments.append({field_name.text : valid_values.text})
                except TypeError as e:
                    print(str(e))
                    pass
        except ( AttributeError , KeyError):
            pass
    return r.text

class TrelloArgument(object):
    """Trello argument passed among with the request, valid values
    should be a list"""
    def __init__(self, name,
                 required,
                 valid_values):
        self.name = name
        self.required = required
        self.valid_values = valid_values

class TrelloRequest(object):
    """Trello Request"""
    def __init__(self, method,
                 path, args):
        self.method = method
        self.path = path
        self.args = args
        self.action = self.path.split("/")[-1]
        required_fields = re.findall("\[(.*?)\]",path)
        if len(required_fields) > 0:
            if required_fields[-1] not in [arg.name for arg in self.args]:
                self.required_objects =required_fields
            else:
                self.required_objects = required_fields[:-1]
        else:
            self.required_objects = None
    def make_request(self, path=None, path_params= None, test_arg=None, **kwargs):
        #parse required identifiers and fields
        if not path:
            path = self.path
        request_args = {}
        #parsing path arguments
        self.path_args =  re.findall("\[(.*?)\]",path)
        #Filling the requested parameters with first available param, unless the param is given
        for arg in self.args:
            if arg.required:
               if arg.name not in self.path_args and arg.name not in kwargs.keys():
                    request_args[arg.name] = arg.valid_values[0]
               elif arg.name not in kwargs.keys():
                   path.replace(arg.name, str(arg.valid_values[0]))
               else:
                   request_args[arg.name] = kwargs[arg.name]
        for key, value in kwargs.items():
            request_args[key] = value
        response = sendRequest(uri=path, method=self.method, params=request_args)
        return response

class CreateRequest(TrelloRequest):
    """Trello Request creating an object. Object type is determined by request's action
    (object name in plural). Each created object is stored within the active_objects list for usage in test scenarios
    method __createobject() creates the new object in Trello, passes its params and returns TrelloObject"""
    req_list = {}
    def __init__(self, method, path, args):
        TrelloRequest.__init__(self, method, path, args)
        if self.action not in [value for key, value in id_dict.items()]:
            pass
        CreateRequest.req_list[self.action] = self
        #Parsing Parent element, can be either in path or in arguments list
        filtered_id_objects = [arg.name for arg in self.args if arg.name in id_dict
                               and arg.required == True]
        if filtered_id_objects:
            self.parent_object = filtered_id_objects[0]
        elif self.required_objects:
            self.parent_object = self.required_objects[-1]
        else:
            self.parent_object = None

    def create_object(self, **kwargs):
        request=  None
        parent_obj_id = None
        parent_obj_type = None
        response_code = None
        response_text = None
        path = self.path
        parent_object_type = self.parent_object
        if parent_object_type:
            if parent_object_type in self.path:
                parent_object = find_object_by(obj_type=path_ids.get(parent_object_type))
                replaced_string =  "[" + parent_object_type + "]"
                path = path.replace(replaced_string, parent_object.obj_id)
                parent_object_type = parent_object.obj_type
                parent_obj_id = parent_object.obj_id
        try:
            request = self.make_request(path=path, **kwargs)
        except Exception as e:
            response_error_message = str(e)
            pass
        try:
            id = self.parse_element(request, "id")
            if not parent_obj_id:
                if parent_object_type:
                    parent_obj_id = self.parse_element(request, parent_object_type)
                else:
                    parent_obj_id = None
                obj = TrelloObject(obj_type=self.action, obj_id=id, parent_obj_id=parent_obj_id)
            else:
                obj = TrelloObject(obj_type=self.action, obj_id=id, parent_obj_id=parent_obj_id)
            active_objects.append(obj)
        except Exception:
            pass

        request_data  = {
            "url" : request.url,
            "data_params" : request.request.body
        } if request else response_error_message

        if request != None:
            response_code = request.status_code
            response_text = request.text
            response_data = {
            "response code" : response_code ,
            "response text" : response_text
        }
        if request != None and obj:
            status = "success"
        else:
            status = "fail"

        object_details = { "id" : obj.obj_id, "obj_type" : obj.obj_type, "parent_obj_id" : obj.parent_obj_id,
                           "parent_object_type": find_object_by(obj_id=obj.parent_obj_id).obj_type
                           if obj.parent_obj_id else None }
        print("Operation : " + "Object Creation" + '\n' +
              "Transaction Endpoint : " + self.path +"\n" +
              "Transaction object : " +  self.action+ "\n" +
              "Status : " + status + "\n" +
              "Created object : " +
              str(object_details) + "\n" +
              "Request : " +  str(request_data) + "\n" +
              "Response :" + str(response_data) + "\n")

        report_columns = ["Operation", "Endpoint", "Status", "Object Type",
                          "Passed Parameters", "Request", "Response Code", "Response Text", "Operated object", "Missed values"]
        test_results.append([ "Object Creation", self.path, status, self.action, str(kwargs), str(request_data),
        response_code, response_text, str(object_details), ""])
        return obj

    def parse_element(self, response, element):
        parsed_value = response.json()[element]
        return parsed_value

class DeleteRequest(TrelloRequest):
    """To be updated"""
    req_list = {}
    def __init__(self, method, path, args):
        TrelloRequest.__init__(self, method, path, args)
        self.delete_object = self.action
        DeleteRequest.req_list[self.delete_object] = self

    def delete(self, object_id):
        request_path = self.path.replace(self.action, object_id)
        self.make_request(path = request_path)

class TransitionRequest(TrelloRequest):

    """TransitionRequest is a request """
    req_list = []
    def __init__(self, method, path, args):
        TrelloRequest.__init__(self, method, path, args)
        self.transition_field = path.split("/")[-1]
        self.transition_object_type = self.required_objects[-1]
        TransitionRequest.req_list.append(self)

    def transpose(self, **kwargs):
        #declaring variables to prevent null pointers
        Success = False
        request = None
        response_text = None
        response_code = None
        transposed_fields = {}
        #get or form the transposed_object
        transposed_object = find_object_by(obj_type=path_ids.get(self.transition_object_type))
        path_id_string = "[" + self.transition_object_type + "]"
        transpose_path = self.path.replace(path_id_string, transposed_object.obj_id)
        if len(self.required_objects) > 1:
            #substituting all required object ids with parent obj ids
            i = len(self.required_objects)
            child_obj_id = transposed_object.obj_id
            while i != 0:
                i = i-1
                substituted_id_type = self.required_objects[i]
                substituted_id_string = "[" + substituted_id_type + "]"
                transpose_path = transpose_path.replace(substituted_id_string, child_obj_id)
                parent_obj_id = find_object_by(obj_id=child_obj_id).parent_obj_id
                child_obj_id = parent_obj_id
        try:
            request = self.make_request(path=transpose_path, **kwargs)
        except Exception as e:
            response_error_message = str(e)
            pass
        ## Check if parent object was changed.
        try:
            object_params = [arg.name for arg in self.args if arg.name in id_dict.keys() and arg.required]
            if object_params:
                new_parent_id = request.json()[object_params[0]]
                transposed_object.parent_obj_id = new_parent_id
            #naming the transposed fields

            for key, value in kwargs.items():
                if key == "value":
                    transposed_fields[self.action] = kwargs.get("value")
                else:
                    transposed_fields[key] = value
        except:
            pass
        missed_values = self.assert_transposition(request, **transposed_fields)
        #Checking all the parameters:
        if request != None and not missed_values:
            Success = True
        request_data  = {
            "url" : request.url,
            "data_params" : request.request.body
        } if request else response_error_message
        if request != None:
            response_code = request.status_code
            response_text = request.text
            response_data = {
                "response code": response_code,
                "response text": response_text
            }

        status = "success" if Success else "failed"
        object_details = {"object type : " : transposed_object.obj_type, "id" : transposed_object.obj_id}
        print("Operation : " + "Transition" + '\n' +
              "Transtion Endpoint : " + self.path +"\n" +
              "Transition object : " +  self.transition_object_type+ "\n" +
              "Status : " + status + "\n" +
              "Transitioned fields : " + str(transposed_fields) + "\n"+
              "Transitioned object : " +
              str(object_details) + "\n" +
              "Request : " +  str(request_data) + "\n" +
              "Response :" + str(response_data) + "\n"+
             "missed values : " + str(missed_values)+ "\n")
        test_results.append([ "Transition", self.path, status, transposed_object.obj_type, str(kwargs), str(request_data),
        response_code, response_text, str(object_details), str(missed_values)])
        return (request, transposed_fields)
    def assert_transposition(self, response, **transposed_fields):
        asserted_values = {}
        response_json = response.json()
        for key, value in transposed_fields.items():
            try:
                response_value = response_json[key]
                asserted_values[key] = str(response_value)
            except KeyError:
                pass
        missed_values = [key for key in transposed_fields.keys() if key not in asserted_values.keys()]
        return missed_values
class TrelloObject(object):
        """TrelloObject is a python object representation of Trello object
        Each Trello object has its id, type, and parent object (if applicable).
        Parent object_id may change during TransitionRequest execution"""
        def __init__(self, obj_type, obj_id, parent_obj_id=None):
            self.obj_type = obj_type
            self.obj_id = obj_id
            self.parent_obj_id = parent_obj_id

        def change_parent_id(self, new_parent_id):
            self.parent_obj_id = new_parent_id

        def delete(self):
            remove_object(self)

class AssertionRequest(TrelloRequest):
    req_list = {}
    def __init__(self, method, path, args) :
        TrelloRequest.__init__(self,method, path, args)
        self.assertion_object = self.action
        AssertionRequest.req_list[self.assertion_object]= self

    def assert_param(self, param_name, expected_value):
        request = self.make_request()
        if request.json()[param_name] == expected_value:
            return True
        else:
            return False

if __name__ == "__main__":
    """First we need to check if all the objects can be created
    Objects have to be created at first to start the tests"""
    name_arg = TrelloArgument("name", True, [ "First Name", "Second Name", "Third Name", "Fourth Name", "Fifth Name"])
    board_create = CreateRequest("POST", "/1/boards", [name_arg])
    board_create.create_object()
    board_arg = TrelloArgument("idBoard", True, [find_object_by(obj_type=id_dict.get("idBoard")).obj_id,
                                                 create_object(id_dict.get("idBoard")).obj_id])
    list_create = CreateRequest("POST", "/1/lists",[ name_arg, board_arg] )
    list_create.create_object()
    list_arg = TrelloArgument("idList", True, [ find_object_by(obj_type=id_dict.get("idList")).obj_id,
                                                create_object(id_dict.get("idList")).obj_id])
    card_create = CreateRequest("POST", "/1/cards", [list_arg, name_arg, board_arg])
    card_create.create_object()
    card_arg = TrelloArgument("idCard", True, [find_object_by(obj_type=id_dict.get("idCard")).obj_id,
                                               create_object(id_dict.get("idCard")).obj_id])
    color_arg = TrelloArgument("color", True, ["black", "orange", "purple", "red", "green", "yellow", "blue","null"])
    label_create = CreateRequest("POST", "/1/labels", [name_arg, color_arg, board_arg])
    label_create.create_object()
    checklist_create = CreateRequest("POST", "/1/checklists", [card_arg, name_arg])
    checklist_create.create_object()
    checkitem_create = CreateRequest("POST", "/1/checklists/[idChecklist]/checkItems", [name_arg])
    checkitem_create.create_object()
    idmodel_arg = TrelloArgument("idModel",True,  [find_object_by(obj_type=id_dict.get("idCard")).obj_id])
# to form a webhook we need an URL accessible with Head request, returning 200 response codes
#    callbackurl_arg = TrelloArgument("callbackURL",True, ["http://localhost:5000"])
#    webhook_create = CreateRequest("POST", '/1/webhooks', [idmodel_arg, callbackurl_arg])
#    webhook_create.create_object()


    """Here we determine and test the transition arguments"""
    pos_arg = TrelloArgument("pos", True, ["bottom", "top"])
    pos_value_arg = TrelloArgument("value", True, pos_arg.valid_values)
    value_arg = TrelloArgument("value", True, ["first string", "second string",
                                               "third string", "fourth string"])
    boolean_value_arg = TrelloArgument("value", True,["true", "false"])
    color_value_arg = TrelloArgument("value", True, color_arg.valid_values)
    name_value_arg = TrelloArgument("value", True, name_arg.valid_values)
    closed_arg = TrelloArgument("closed", False, boolean_value_arg.valid_values)
    desc_arg = TrelloArgument("desc", False, ["first description", "second description",
                                              "third description", "fourth description"])
    desc_value_arg = TrelloArgument("value", False, desc_arg.valid_values)
    pos_arg = TrelloArgument("pos", True, ["bottom", "top"])
    pos_value_arg = TrelloArgument("value", True, pos_arg.valid_values)
    subscribed_arg = TrelloArgument("subscribed", False, boolean_value_arg.valid_values)
    checkitem_state_arg = TrelloArgument("state", False,["complete", "false", "incomplete", "true"])
    checkitem_state_value_arg = TrelloArgument("value", True, checkitem_state_arg.valid_values)
    checklist_arg = TrelloArgument("idChecklist", False,
                                   [find_object_by(obj_type=id_dict.get("idChecklist")).obj_id,
                                    create_object(id_dict.get("idChecklist"))])


    """Here we're forming the TransitionRequest objects, which can be used to
    form the transition requests with passed parameters"""
    TransitionRequest("PUT","/1/cards/[card id or shortlink]/checklist/[idChecklist]/checkItem/[idCheckItem]/name",
                      [value_arg])
    TransitionRequest("PUT", "/1/cards/[card id or shortlink]/dueComplete", [boolean_value_arg])
    TransitionRequest("PUT", "/1/labels/[idLabel]", [color_arg, name_arg])
    TransitionRequest("PUT", "/1/labels/[idLabel]/color", [color_value_arg])
    TransitionRequest("PUT", "/1/boards/[board_id]", [name_arg, closed_arg, desc_arg, subscribed_arg])
    TransitionRequest("PUT", "/1/boards/[board_id]/myPrefs/emailPosition", [pos_value_arg])
    TransitionRequest("PUT", "/1/boards/[board_id]/myPrefs/showListGuide", [boolean_value_arg])
    TransitionRequest("PUT", "/1/boards/[board_id]/myPrefs/showSidebar", [boolean_value_arg])
    TransitionRequest("PUT", "/1/boards/[board_id]/myPrefs/showSidebarActivity",[boolean_value_arg])
    TransitionRequest("PUT", "/1/boards/[board_id]/myPrefs/showSidebarBoardActions", [boolean_value_arg])
    TransitionRequest("PUT", "/1/boards/[board_id]/myPrefs/showSidebarMembers", [boolean_value_arg])
    TransitionRequest("PUT", "/1/boards/[board_id]/name", [name_value_arg])
    TransitionRequest("PUT", "/1/boards/[board_id]/subscribed", [boolean_value_arg])
    TransitionRequest("PUT", "/1/cards/[card id or shortlink]/checklist/[idChecklist]/checkItem/[idCheckItem]/name",
                      [name_value_arg])
    TransitionRequest("PUT", "/1/cards/[card id or shortlink]/checklist/[idChecklist]/checkItem/[idCheckItem]/pos",
                      [pos_value_arg])
    TransitionRequest("PUT", "/1/cards/[card id or shortlink]/checklist/[idChecklist]/checkItem/[idCheckItem]/state",
                      [checkitem_state_value_arg])
    TransitionRequest("PUT", "/1/cards/[card id or shortlink]/checklist/[idChecklistCurrent]/checkItem/[idCheckItem]",
                      [pos_arg, checkitem_state_arg, name_arg, checklist_arg])
    TransitionRequest("PUT", "/1/cards/[card id or shortlink]/closed", [boolean_value_arg] )
    TransitionRequest("PUT", "/1/cards/[card id or shortlink]/desc", [value_arg])
    TransitionRequest("PUT", "/1/cards/[card id or shortlink]/name", [name_value_arg])
    TransitionRequest("PUT","/1/cards/[card id or shortlink]/pos", [pos_value_arg])
    TransitionRequest("PUT", "/1/cards/[card id or shortlink]/subscribed", [boolean_value_arg])
    TransitionRequest("PUT", "/1/checklists/[idChecklist]", [name_arg, pos_arg])
    TransitionRequest("PUT", "/1/checklists/[idChecklist]/name", [name_value_arg])
    TransitionRequest("PUT", "/1/checklists/[idChecklist]/pos", [pos_value_arg])
    TransitionRequest("PUT", '/1/labels/[idLabel]', [name_arg, color_arg])
    TransitionRequest("PUT", "/1/labels/[idLabel]/color", [color_value_arg])
    TransitionRequest("PUT","/1/labels/[idLabel]/name", [name_value_arg])
    TransitionRequest("PUT", "/1/lists/[idList]", [name_arg, closed_arg, pos_arg, subscribed_arg])
    TransitionRequest("PUT", "/1/lists/[idList]/name", [name_value_arg])
    TransitionRequest("PUT", "/1/lists/[idList]/pos", [pos_value_arg])
    TransitionRequest("PUT", "/1/lists/[idList]/subscribed", [boolean_value_arg])
    i = 0
    for key, value in CreateRequest.req_list.items():
        for arg in value.args:
            for valid_value in arg.valid_values:
                request_data = {}
                request_data[arg.name] = valid_value
                value.create_object(**request_data)
                i = i+1
    print ("Creation operations commited : " + str(i))
    #Transitioning each object field
    i = 0
    for transition_request in TransitionRequest.req_list:
        for arg in transition_request.args:
            for valid_value in arg.valid_values:
                d = {arg.name: valid_value}
                try:
                    transition_request.transpose(**d)
                except Exception as e:
                    print (str(e))
                i = i+1

    print ("Transition operations commited: " + str(i))
    print ("Objects created during test run :")
    for object in active_objects:
        print("object id " + object.obj_id, "object type : "+ object.obj_type, "object parent id : "
              +str(object.parent_obj_id))

    #Generating Excel report
    TestReport(report_columns, test_results)