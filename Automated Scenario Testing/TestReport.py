import xlsxwriter
import os
from time import gmtime, strftime
cwd = os.getcwd()
class TestReport(object):
    """Excel report, takes column names and values as a list and generates a report
    in excel file, stored in "reports" directory"""
    def __init__(self, column_names, values):
        self.file = self.create_workbook()
        self.worksheet = self.file.add_worksheet("Report Results")
        self.add_headers(column_names)
        self.fill_report(values)
        self.file.close()
    def create_workbook(self):
        try:
            timestamp = strftime("%d-%b-%Y-%H-%M-%S", gmtime())
            wb = xlsxwriter.Workbook(os.path.join(cwd, "reports", "test_report_" + timestamp + ".xlsx"))
            return wb
        except Exception as e:
            print("Failed to generate report file\n" + str(e))
            pass
    def add_headers(self, columns):
        for i, column in enumerate(columns):
            self.worksheet.write(0, i, column)
    def fill_report(self, values):
        for i, value in enumerate(values):
            self.worksheet.write_row(i+1,0, value)
if __name__ == "__main__":
    tr= TestReport(["column1", "column2"], [["value1", "value2"], ["value2"]])

