from requests_oauthlib import OAuth1
from urllib.parse import parse_qs
import requests
import json
from json import JSONDecodeError

auth_token = input("Enter API token:")
api_key = input("Enter API key:")

if auth_token == "" or api_key == "":
    ##TO be removed on testset
    auth_token = "e63a7890c8b1379a0b9edfb2a8ff1795d4159254ab6f72207b789e0095ff12af"
    api_key = "10932f5899164feca42ac6019a3504f1"
    api_secret = "55a251e6687b7c98020a79dd413edc7500197e48984077519fe575d6df61bfee"
url_base = "https://trello.com"


def oauth_authorize_user(api_key, api_secret):
    """For the Functional testing we will use the standard token-key authentication"""
    """generating token using OAuth keys"""
    url = "/1/OAuthGetRequestToken"
    url = url_base + url
    callback_url = url_base + "/1/OAuthAuthorizeToken"
    access_token_url = url_base + "/1/OAuthGetAccessToken"

    oauth = OAuth1(api_key, client_secret=api_secret)
    r = requests.get(url=url, auth=oauth, allow_redirects=True)
    parser = parse_qs(r.content)
    try:
        ## parse OAuth token + secret
        oauth_token = str(parser.get(b"oauth_token")[0])
        oauth_token_secret = str(parser.get(b"oauth_token_secret")[0])
        ##        print (oauth_token)
        ##        print (oauth_token_secret)
        oauth = OAuth1(api_key, api_secret, oauth_token, oauth_token_secret)
        r = requests.get(url=callback_url, auth=oauth)
    except AttributeError as e:
        print ("failed to parse token, please check the connection and the api keys\n")
        print (str(e))
        return None
    return oauth

def authorize_user(scope="read,write", expiration="never", name="Py Tester Script"):
    """token generation to authenticate the testing framework to perform operations"""
    url_base = "https://trello.com"
    params = {"key": api_key, "name": name,
              expiration: expiration, "response_type": "token", "callback_method": "postMessage"}
    r = sendRequest("/1/authorize", "GET", params=params)
    return r

def sendRequest(uri, method, auth=None,
                params={}):
    s = requests.Session()
    """ adding api_key and auth_token to header params """
    params['key'] = api_key
    params['token'] = auth_token
    """  URL concatenate """
    url = url_base + uri
    try:
        params = json.loads(str(params))
    except JSONDecodeError:
        pass
    r = requests.Request(method=method, url=url,
                         params=params,
                         auth=auth)
    """  preparing the request """
    prepped = r.prepare()
    try:
        resp = s.send(prepped)
    except Exception as e:
        print(str(e))
    return resp