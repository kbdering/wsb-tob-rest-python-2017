import tkinter as tk
from TextOper import *

"""Package GuiTextDisplay contains GUI elements to maintain 
the communication beteween the aplication and the user via interface.
Each element is bound to its passed variable ( tkinter.StringVar instance )
and contains the tkinter.Label for variable description

package object instances :

 TextEntry: single line string input
 TextField: multi line string/json input
 TextLabel: single/multi line string display
 DropDown : OptionMenu dropdown to display table/list contains"""

class TextEntry(tk.Entry):
    """TextEntry class represents tkinter.Entry instance with context bound to StringVar param.
    Single line user input
    """

    def __init__(self, param,
                 label_text, master,
                 column, row):
        self.param = param
        self.label = tk.Label(master, text=label_text)
        self.label.grid(column=column, row=row)
        self.init_var = self.param.get()
        tk.Entry.__init__(self, master, textvariable=self.param, width=75)
        self.grid(column=column + 1, row=row, columnspan=4)
        self.bind("<Key>", self.save_param)
        self.bind("<FocusOut>", self.save_param)

    def save_param(self, *args):
        self.param.set(self.get())


class TextField(tk.Text):
    """TextField class, inheriting tkinter.Text object represents active text field."""

    def __init__(self, param,
                 label_text, master,
                 column, row):
        self.label = tk.Label(master, text=label_text)
        self.label.grid(column=column, row=row)
        self.var = param
        self.init_value = self.var.get()
        tk.Text.__init__(self, master, width=75, height=20)
        self.insert("0.0", self.init_value)
        self.grid(column=column + 1, row=row, columnspan=6)
        self.bind("<FocusOut>", self.update_param)
        self.var.trace("w", self.replace_text)
        self.scrollbar = tk.Scrollbar(master)
        self.scrollbar.grid(column=column + 6, row=row, sticky=tk.E + tk.N + tk.S)
        self.scrollbar.config(command=self.yview)
        self.config(yscrollcommand=self.scrollbar.set)

    def get_text(self):
        return self.get("0.0", tk.END)

    def update_param(self, *args):
        self.var.set(self.get_text())
        self.update_idletasks()

    def replace_text(self, *args):
        self.delete("0.0", tk.END)
        text = str(jsonfy(self.var.get()))
        self.insert("0.0", text.rstrip())


class TextLabel(tk.Label):
    def __init__(self, param,
                 label_text, master,
                 column, row):
        self.param = param
        self.init_param = param.get()
        self.label = tk.Label(master, text=label_text)
        self.label.grid(column=column, row=row)
        param.trace("w", self.update_param)
        tk.Label.__init__(self, master, text=self.init_param)
        self.grid(column=column + 1, row=row, columnspan=1, sticky=tk.W)

    def update_param(self, *args):
        self.config(text=self.param.get())


class DropDown(tk.OptionMenu):
    def __init__(self, master, row, column, param,
                 values_list):
        tk.OptionMenu.__init__(self, master, param, *values_list)

        self.grid(column=column, row=row)
