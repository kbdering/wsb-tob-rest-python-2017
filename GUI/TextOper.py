import json
import random
import string

"""A set of most common text operations"""

def text_wrap(text, line_length):
    formatted_text = ""
    line_max_length = line_length
    current_line_length = 0
    for word in text.split(" "):
        if len(word) + current_line_length > line_max_length:
            current_line_length = 0
            current_line_length += len(word)
            formatted_text += "\n" + word
        else:
            current_line_length += len(word)
            formatted_text += " " + word
    return formatted_text

def jsonfy(text):
    try:
            return json.dumps(json.loads(text), indent=1)
    except:
            return text

def generate_string(length):
    try:
        return (''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(int(length))))
    except ValueError:
        return None
