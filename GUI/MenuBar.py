import tkinter as tk
from tkinter import messagebox, filedialog
import pickle



def about():
    messagebox.showinfo("About Application", "This application was made by Anna Serafin and Jakub Dering,\n"
                                             "students of Wyższa Szkoła Bankowa w Gdyni.")
class Menubar(tk.Menu):
    """
    Menu bar containing main actions related to Test Plan
    -Test Plan Execution
    - Saving
    - Opening 
    - Creating new test plan
    """
    def __init__(self, root):

        tk.Menu.__init__(self, root)
        testplanmenu = tk.Menu(self, tearoff = 0)
        testplanmenu.add_command(label="Execute Test Plan", command=root.execute_test_plan)
        testplanmenu.add_separator()
        testplanmenu.add_command(label = "Create new", command = root.new_test_plan)
        testplanmenu.add_command(label = "Open Test Plan", command = self.load_test_plan)
        testplanmenu.add_command(label="Save Test Plan", command = self.save_test_plan)
        testplanmenu.add_separator()
        testplanmenu.add_command(label= "Exit", command = root.quit)
        self.add_cascade(label="Test Plan", menu=testplanmenu)
        aboutusmenu = tk.Menu(self, tearoff= 0)
        aboutusmenu.add_command(label = "About tool", command = about)
        self.add_cascade(label = "About", menu = aboutusmenu)

    def save_test_plan(self):
        savefile = filedialog.asksaveasfilename(filetypes = [("Trello TestPlan", ".tp")])
        if not savefile.endswith(".tp"):
            savefile = savefile + ".tp"
        try:
            with open(savefile, "wb") as f:
                #copying test plan
                test_plan_copy = self.master.test_plan.test_steps[:]
                for step in test_plan_copy:
                    step.pack()
                pickle.dump(test_plan_copy, f)
                for step in test_plan_copy:
                    step.repack()
        except Exception as e:
            messagebox.showwarning("Save File", "Failed to save the file, reason : \n" + str(e))
        self.master.menu_canvas.callback()

    def load_test_plan(self):
        filename = filedialog.askopenfile(mode='rb', filetypes=[("Trello TestPlan", '.tp')])
        try:
                loaded_test_plan = pickle.load(filename)
                for step in loaded_test_plan:
                    step.repack()
                self.master.test_plan.load(loaded_test_plan)

        except Exception as e:
            messagebox.showwarning("Open File", "Could not load the test plan. Please select proper file or try again"
                                                "\n error message:\n" + str(e))
        self.master.menu_canvas.callback()
