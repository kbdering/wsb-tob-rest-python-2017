#!/usr/bin/python3
# -*- coding: utf-8 -*-
from GuiCanvas import *
from MenuBar import *
from TestStep import *
from TestPlan import *

class tkApp(tk.Tk):

    def __init__(self):
        tk.Tk.__init__(self, screenName="Trello API Test Plan Executer")

        self.test_plan = TestPlan()
        self.menubar = Menubar(self)
        self.config(menu=self.menubar)
        self.menu_canvas = TestStepsMenu(self, self.test_plan)
        self.step_canvas = StepCanvas(self, self.test_plan)
        self.bottom_frame = self.addStepButtonFrame(self)
        self.title("Trello API Test Plan Executer")
        self.add_menu_scrollbar()
        self.add_step_canvas_scrollbars()
        self.configure_rows()

    def execute_test_plan(self):
        self.test_plan.execute()

    def delete_step(self, step):
        self.test_plan.delete_step(step)

    def draw_step(self, step):
        self.step_canvas.draw_step(step)

    def add_step(self, step):
        self.test_plan.add_test_step(step)

    def new_test_plan(self):
        self.test_plan.load([Step(name="New Test Step")])
        self.menu_canvas.callback()

    def configure_rows(self):
        self.grid_rowconfigure(0, weight =1 )
        self.grid_rowconfigure(1, weight =0 )
        self.grid_columnconfigure(0, weight =0)
        self.grid_columnconfigure(1, weight =0)
        self.grid_columnconfigure(4, weight =1)

    def add_menu_scrollbar(self):
        self.scrollbar = tk.Scrollbar(self)
        self.scrollbar.grid(column=0, row=0,
                            rowspan=1, sticky=tk.N + tk.S+tk.E)
        self.menu_canvas.config(scrollregion=self.menu_canvas.bbox("all"))
        self.scrollbar.config(command=self.menu_canvas.yview)
        self.update()
        self.menu_canvas.config(yscrollcommand=self.scrollbar.set)

    def add_step_canvas_scrollbars(self):
        self.vscrollbar = tk.Scrollbar(self, orient=tk.VERTICAL, command = self.step_canvas.yview)
        self.hscrollbar = tk.Scrollbar(self, orient=tk.HORIZONTAL, command = self.step_canvas.xview)
        self.hscrollbar.grid(row= 1, column = 4, columnspan=5, sticky = tk.W+tk.E+tk.S)
        self.vscrollbar.grid(row=0, column=9, rowspan =2, sticky = tk.S+tk.N+tk.E )
        self.step_canvas.config(yscrollcommand = self.vscrollbar.set, xscrollcommand = self.hscrollbar.set, scrollregion=self.step_canvas.bbox("all"))

    class addStepButtonFrame(tk.Frame):

        def __init__(self, master):
            tk.Frame.__init__(self)
            self.grid(column=0, row=1, sticky=tk.W + tk.S)
            self.add_blank_step_button = tk.Button(self, text="add new step", command=self.add_blank_step)
            self.add_blank_step_button.grid(column=0, row=0, sticky=tk.W + tk.S)
            self.config(padx=1, pady=1)

        def add_blank_step(self):
            self.master.test_plan.add_blank_step()
            self.master.menu_canvas.callback()

if __name__ == "__main__":
    t = tkApp()
    t.mainloop()
