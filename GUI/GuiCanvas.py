from TestStep import Step
from StepButton import *

from GuiTextDisplay import *

"""Menu Canvas designed to display and mantain the test plan """

class TestStepsMenu(tk.Canvas):
    """tkCanvas holding all the current test steps.
    Canvas object required to the grid configuration with scrollbars"""
    def __init__(self, parent, test_plan):
        tk.Canvas.__init__(self, parent)
        self.grid(column=0, row=0, columnspan=1, sticky=tk.N + tk.W + tk.S)
        # user will be able to pass the extracted JSON values into param, which can be used in later steps
        self.steps_frame = self.StepsFrame(self, test_plan)
        self.window = self.create_window(0, 0, window=self.steps_frame, anchor="nw")

    def callback(self):
        self.steps_frame.draw_buttons()
    class StepsFrame(tk.Frame):
        """a frame holding the StepButtons"""
        def __init__(self, TestStepsMenu, test_plan):
            self.test_plan = test_plan
            tk.Frame.__init__(self, TestStepsMenu)
            self.grid(column=0, row=0, sticky=tk.N + tk.W)
            self.master = TestStepsMenu
            self.draw_buttons()
            self.config(highlightcolor="BLACK", highlightbackground="BLACK", bd=1, relief=tk.GROOVE)

        def draw_buttons(self):
            for child in self.winfo_children():
                child.grid_forget()
            for row, step in enumerate(self.test_plan.test_steps):
                StepButton(step, self, row)

class StepCanvas(tk.Canvas):
        """tkCanvas to display the step details
        Canvas object required to the grid configuration with scrollbars"""
        def __init__(self, master, test_plan, *args, **kwargs):
            tk.Canvas.__init__(self, master)
            self.test_plan = test_plan
            self.grid(column=1, row=0, rowspan=1, columnspan=5, sticky=tk.N + tk.S + tk.W + tk.E)
            self.request_frame = self.StepRequestFrame(self)
            self.create_window(0, 0, anchor="nw", window=self.request_frame)
            self.config(scrollregion=self.bbox("all"))

        def clear_frame(self):
            for child in self.request_frame.winfo_children():
                child.grid_forget()

        def draw_step(self, step):
            self.test_plan.active_step = step
            self.clear_frame()
            self.request_frame.StepRequestFrameNavButtons(self.request_frame)
            # Request Fields
            TextEntry(master=self.request_frame, column=0, row=1, param=step.name, label_text="Step Name")
            TextEntry(master=self.request_frame, column=0, row=2, param=step.description, label_text="Step Description")
            TextLabel(master=self.request_frame, column=0, row=3, param=step.status, label_text="Status")
            DropDown(master=self.request_frame,  column=5, row=4, param=step.request_method, values_list=Step.methods)
            TextEntry(master=self.request_frame, column=0, row=4, param=step.request_url, label_text="Request URL")
            TextField(master=self.request_frame, column=0, row=5, param=step.request_body, label_text="Request Body")
            TextEntry(master=self.request_frame, column=0, row=6, param=step.json_extractor, label_text="JSON path extractor (Optional)")
            TextEntry(master=self.request_frame, column=0, row=7, param=step.json_extractor_param_name, label_text="Target Param (Optional)")
            TextEntry(master=self.request_frame, column=0, row=8, param=step.assertion_script, label_text="Assertion Script")
            # Response Fields
            TextLabel(master=self.request_frame, column=0, row=9, param=step.extracted_param, label_text="Extracted Param")
            TextLabel(master=self.request_frame, column=0, row=10, param=step.response_code, label_text="Response Code")
            TextField(master=self.request_frame, column=0, row=11, param=step.response_data, label_text="Response Data")
            self.config(scrollregion=self.bbox("all"))

        def execute_step(self):
            self.test_plan.active_step.execute(self.test_plan.test_params)

        def delete_step(self):
            self.test_plan.delete_step(self.test_plan.active_step)
            self.master.menu_canvas.callback()

        class StepRequestFrame(tk.Frame):
            """ StepRequestFrame is the frame drawing the Step details
            Initially the step is blank and loaded on step change/ update/ load"""
            def __init__(self, step_canvas):
                tk.Frame.__init__(self, step_canvas)
                self.step = None
                self.grid(row=0, column=0, columnspan=2, sticky=tk.E + tk.N + tk.W + tk.S)
                self.StepRequestFrameNavButtons(self)

            def step_update(self, step):
                self.step = step
                self.master.draw_step(step)

            def execute_step(self):
                self.master.execute_step()

            def delete_step(self):
                self.master.delete_step()

            class StepRequestFrameNavButtons(tk.Frame):
                """Test Step navigation buttons"""
                def __init__(self, request_frame):
                    tk.Frame.__init__(self, request_frame)
                    # adding action buttons
                    tk.Button(self, text="Delete Step", command=request_frame.delete_step).grid(column=1, row=0,
                                                                                                sticky=tk.W)
                    tk.Button(self, text="Execute", command=request_frame.execute_step).grid(column=3, row=0,
                                                                                             sticky=tk.E)
                    self.grid(row=0, column=0, columnspan=6)

