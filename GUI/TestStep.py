import string
import tkinter as tk

from TestRequest import TestRequest


class Step(object):
    step_statuses = ["pending", "in_progress", "failed", "success", "assertion_error"]
    methods = ["GET", "PUT", "POST", "DELETE", "HEAD", "OPTIONS", "PATCH", "CONNECT", "TRACE"]
    def __init__(self, **kwargs):
        """ Step is our request and a test step. By now we focus on Json requests and responses only
         but these can be modified on demand for any request type
          Each step containts its response which populates on Step execution.  """
        self.name = tk.StringVar()
        self.description =tk.StringVar()
        # url to be reached by current step on execution
        self.request_url = tk.StringVar()
        # requests method
        self.request_method= tk.StringVar()
        self.request_body = tk.StringVar()
        # request params
        self.params = tk.StringVar()
        # response data
        self.response_data = tk.StringVar()
        self.response_code = tk.StringVar()
        # json extractor script (to parse specific parameter in json response
        self.comments = tk.StringVar()

        self.json_extractor = tk.StringVar()
        self.json_extractor_param_name = tk.StringVar()
        self.extracted_param = tk.StringVar()      # initially the status is pending
        self.status = tk.StringVar()
        self.status.set(Step.step_statuses[0])
        self.assertion_script = tk.StringVar()
        self.asserted_value = tk.StringVar()
        #override values passed in arguments
        for key, arg in kwargs.items():
            if key in self.__dict__.keys():
                #not the best way to do it, but at least it works
                eval_string = "self." + key  + ".set(arg)"
                eval(eval_string)

    def execute(self, test_plan_params):
        #setting status to in_progress
        self.status.set(Step.step_statuses[1])
        """Executing step with current test plan params"""
        #substituting fields with extracted params
        for param in [self.request_url, self.request_body,
                      self.name, self.description, self.json_extractor,
                      self.json_extractor_param_name, self.assertion_script]:
            param.set( self.substitute_param(param.get(), test_plan_params))
        request = TestRequest(self)
        try:
          request.send()
          request.update_step()
          request.assert_request()
          request.parse_response(test_plan_params)
        except Exception as e:
            self.status.set(Step.step_statuses[2])
            self.response_data.set("failed to execute this step. Error message : " + str(e))

            pass
        #change step status on success
        if self.status.get() == Step.step_statuses[1]:
            self.status.set(Step.step_statuses[3])

    def substitute_param(self, param, test_plan_params):
        return string.Template(param).safe_substitute(**test_plan_params)

    def pack(self):
            #Pickle module doesn't support _tkinter app storage,
            # hence converting values into strings, these will be re-converted on re-pack function
            for attr, value in vars(self).items():
                setattr(self, attr, value.get())

    def repack(self):
        for attr, value in vars(self).items():
            setattr(self ,attr, tk.StringVar())
            eval_string = "self." + attr + ".set(value)"
            eval(eval_string)