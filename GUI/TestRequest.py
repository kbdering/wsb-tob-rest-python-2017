import requests
from jsonpath_rw import parse
from requests import Session
from TextOper import *


class TestRequest(requests.Request):

    def __init__(self, step):

        self.step = step
        json_body = None
        try:
            json_body = json.loads(step.request_body.get())
        except:
            pass
        try:
            requests.Request.__init__(self, method=step.request_method.get(),
                                      json=json_body, url=self.step.request_url.get() )
        except Exception as e:
            #Fail step on any exception
            self.step.status.set(self.step.step_statuses[2])
            self.step.response_code.set("Failed to prepare request. Reason: \n" +
                                        text_wrap(str(e)), 40)

    def parse_response(self , test_plan_values):

        jsonpath_string = self.step.json_extractor.get()
        if jsonpath_string != "":
           try:
                parser = parse(jsonpath_string)
                response_data = self.response.json()
                dumped_response_data = json.dumps(response_data)
                response_data = parser.find(json.loads(dumped_response_data))
                extracted_param = response_data[0].value
                param_name = self.step.json_extractor_param_name.get()
                test_plan_values[param_name] = extracted_param
                self.step.extracted_param.set(extracted_param)
           except Exception as e:
                self.step.extracted_param.set("Failed to extract json param. \n"
                                              + text_wrap(str(e), 40))
        else:
            self.step.extracted_param.set("")

    def send(self):
        s = Session()
        try:
            self.response = s.send(self.prepare())
        except Exception as e:
            self.step.status.set(self.step.step_statuses[2])
            self.step.response_code.set("Failed to prepare request. Reason: \n"
                                        + text_wrap(str(e), 40))
            return None
        return

    def update_step(self):
        self.step.response_code.set(self.response.status_code)
        self.step.response_data.set(self.response.text)

    def assert_request(self):
        try:
            eval_string = self.step.assertion_script.get()
            if eval_string != "":
                if eval(eval_string):
                    return True
                else:
                    self.step.status.set(self.step.step_statuses[4])
                    return False
        except Exception as e:
            self.step .status.set(self.step.step_statuses[4])
            self.step.response_code.set("Failed to assert the request. \n"
                                        + text_wrap(str(e), 40))