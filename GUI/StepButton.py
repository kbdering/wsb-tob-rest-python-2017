import os
import sys
import tkinter as tk
from tkinter import messagebox

from TestStep import Step


class StepButton(tk.Button):
    """"StepButton represents the graphical status and redirection of test Step.
     Icon drawn on the left represents the Step status and the displayed text reflects the step name
     or the method and request URL if step remains unnamed. The attributes are bound dynamically and update
     in real time """
    status_images = {}
    def get_status_image(image_name):
        try:
            f = os.getcwd() + "\\images\\" + image_name + ".gif"
            return f
        except FileNotFoundError:
            messagebox.showerror("status image not found",
                                 "Failed to open the image " + str(image_name) +
                                 " The application will close")
            sys.exit()
    # loading button images and storing them in ram memory for faster access
    for status in Step.step_statuses:
        status_images[status] = get_status_image(status)

    def __init__(self, step, parent, row):

        self.step = step
        self.master = parent
        if self.step.name.get() == "":
            button_title = self.step.request_method.get() + " / " + self.step.request_url.get()
        else:
            button_title = self.step.name.get()
        self.image = tk.PhotoImage(file=StepButton.status_images.get(step.status.get()))
        tk.Button.__init__(self, parent, text=button_title,
                           command=lambda: self.action_cmd(step), compound=tk.LEFT,
                           image=self.image)

        self.grid(row=row, column=0, sticky=tk.W)

        # On variables change, redraw the button :

        self.step.name.trace("w", self.redraw)
        self.step.request_url.trace("w", self.redraw)
        self.step.request_method.trace("w", self.redraw)
        self.step.status.trace("w", self.redraw)

    def action_cmd(self, step):
        self.master.master.master.draw_step(step)
        self.redraw()

    def redraw(self, *args):
        self.image = tk.PhotoImage(file=StepButton.status_images.get(self.step.status.get()))
        if self.step.name.get() == "":
            button_title = self.step.request_method.get() + " / " + self.step.request_url.get()
        else:
            button_title = self.step.name.get()
        self.config(image=self.image, text=button_title)
        self.update_idletasks()
