from TestStep import *

class TestPlan(object):

    """TestPlan represents a set of Test Steps"""
    def __init__(self, *test_steps):


        self.active_step = test_steps[0] if test_steps else None
        self.test_params = {}
        self.test_steps = []
        for test_step in test_steps:
            self.test_steps.append(test_step)

        #For new Plan add sample Step
        if len(test_steps) == 0:
            self.add_test_step(
                Step(name="Sample Step (click me!)", description="Test Plan Description",
                                    request_url="http://url to be sampled",
                                    request_body="Use ${test_plan_param} syntax to pass any variable extracted from \n \
                                    previous steps",
                                    json_extractor=" Use $. to get access to response body. (jsonpath_rw atomic syntax)",
                                    json_extractor_param_name="Name of the extracted parameter",
                                    request_method="Request Method",
                                    assertion_script="Py script for request assertion (use self.response to assert the \
                                    requests response)"))

    def add_test_step(self, test_step):
        self.test_steps.append(test_step)

    def add_blank_step(self):
        self.test_steps.append(Step(name="New Test Step"))

    def delete_step(self, step):
        self.test_steps.remove(step)

    def load(self, test_steps):
        self.test_steps =  test_steps


    def execute(self):
        for step in self.test_steps:
            step.execute(self.test_params)

if __name__ == "__main__":
    TestPlan(Step())