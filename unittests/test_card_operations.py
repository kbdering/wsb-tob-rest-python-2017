import unittest
from card_operations import *

class BoardDetails(unittest.TestCase):
    def setUp(self):
        self.name = nonce(10)

    # add new card, make sure it's been posted

    def test_post_card(self):
        result_post = post_card(self.name)
        self.assertEqual(result_post.status_code, 200)
        self.assertEquals(len(self.name), 10)
        self.assertIn(result_post.json()['name'], self.name)

    # add new card, get card's details

    def test_get_details(self):
        result_post = post_card(self.name)
        card_id = result_post.json()['id']
        result_get = get_card(card_id)
        self.assertEqual(result_get.status_code, 200)
        self.assertEquals(len(self.name), 10)
        self.assertIn(result_get.json()['id'], card_id)
        self.assertTrue('name', self.name)

    # add new card, delete it, make sure it's been deleted

    def test_delete_card(self):
        result_post = post_card(self.name)
        card_id = result_post.json()['id']
        result_delete = delete_card(card_id)
        self.assertEqual(result_delete.status_code, 200)
        self.assertNotIn(result_delete.json(), ['id'])


if __name__ == '__main__':
    unittest.main()
