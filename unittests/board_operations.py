import string
import random
import requests
from settings import *

''' Test Scenario: Create a board, a list and a card. Get their details.
Delete the card.

'''

#create random name

nonce = lambda x: ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10 if not x else x))

# create a board
def post_board(board_name):
    url = "https://api.trello.com/1/boards"
    params = {"key": key,
              "token": token,
              "name": board_name}
    return requests.post(url=url, params=params)

# get board's details
def get_board(board_id):
    url = "https://trello.com/1/boards/" + board_id
    params = {"key":key,
          "token":token}
    return requests.get(url=url, params=params)

# create a list
def post_list(board_id, list_name):
    url = "https://api.trello.com/1/lists"
    params = {"key": key,
              "token": token,
              "idBoard": board_id,
              "name": list_name}
    return requests.post(url=url, params=params)

# get list's details
def get_list(id_list):
    url = "https://api.trello.com/1/lists/" + id_list
    params = {"key": key,
              "token": token}

    return requests.get(url=url, params=params)

# create a card
def post_card(id_list):
    url = "https://api.trello.com/1/cards"
    params = {"key": key,
          "token": token,
          "name": 'new card',
          "idList": id_list}
    return requests.post(url=url, params=params)

# get card's details
def get_card(card_id):
    url = "https://trello.com/1/cards/" + card_id

    params = {"key":key,
          "token":token}

    return requests.get(url=url, params=params)


# delete the card
def delete_card(card_id):
    url = "https://trello.com/1/cards/" + card_id

    params = {"key":key,
          "token":token}

    return requests.delete(url=url, params=params)

