import unittest
from board_operations import *


class BoardDetails(unittest.TestCase):
    def setUp(self):
        self.name = nonce(10)


    def test_post_board(self):
        result_post_board = post_board(self.name)
        self.assertEqual(result_post_board.status_code, 200)
        self.assertEqual(len(self.name), 10)
        self.assertIn(result_post_board.json()['name'], self.name)
        

    def test_get_board(self):
        result_post = post_board(self.name)
        board_id = result_post.json()['id']
        result_get_board = get_board(board_id)
        self.assertEqual(result_get_board.status_code, 200)
        self.assertEqual(len(self.name), 10)
        self.assertIn(result_get_board.json()['name'], self.name)
        

    def test_post_list(self):
        result_post = post_board(self.name)
        board_id = result_post.json()['id']
        result_post_list = post_list(board_id, list_name=self.name)
        self.assertEqual(result_post_list.status_code, 200)
        self.assertEqual(len(self.name), 10)
        self.assertIn(result_post_list.json()['name'], self.name)


    def test_get_list(self):
        result_post = post_board(self.name)
        board_id = result_post.json()['id']
        result_post_list = post_list(board_id, list_name=self.name)
        id_list = result_post_list.json()['id']
        result_get_list = get_list(id_list)
        self.assertEqual(result_get_list.status_code, 200)
        self.assertEqual(len(self.name), 10)
        self.assertIn(result_get_list.json()['name'], self.name)
        

    def test_post_card(self):
        result_post = post_board(self.name)
        board_id = result_post.json()['id']
        result_post_list = post_list(board_id, list_name=self.name)
        id_list = result_post_list.json()['id']
        result_post_card = post_card(id_list)
        self.assertEqual(result_post_card.status_code, 200)
        self.assertEqual(len(self.name), 10)
        self.assertTrue('new card')
        

    def test_get_card(self):
        result_post = post_board(self.name)
        board_id = result_post.json()['id']
        result_post_list = post_list(board_id, list_name=self.name)
        id_list = result_post_list.json()['id']
        result_post_card = post_card(id_list)
        card_id = result_post_card.json()['id']
        result_get_card = get_card(card_id)
        self.assertEqual(result_get_card.status_code, 200)
        self.assertEqual(len(self.name), 10)
        self.assertIn(result_get_card.json()['id'], card_id)
        self.assertTrue('name', self.name)
        

    def test_delete_card(self):
        result_post = post_board(self.name)
        board_id = result_post.json()['id']
        result_post_list = post_list(board_id, list_name=self.name)
        id_list = result_post_list.json()['id']
        result_post_card = post_card(id_list)
        card_id = result_post_card.json()['id']
        result_delete_card = delete_card(card_id)
        self.assertEqual(result_delete_card.status_code, 200)
        self.assertNotIn(result_delete_card.json(), ['id'])



if __name__ == '__main__':
    unittest.main()
