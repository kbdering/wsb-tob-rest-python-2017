#!/usr/bin/python3
# -*- coding: utf-8 -*-
import unittest
from board_details import *

class BoardDetails(unittest.TestCase):
    def setUp(self):
        self.name = nonce(10)

    # create new board, verify it's been created
    def test_post_board(self):
        result_post = post_board_name(self.name)

        self.assertEqual(result_post.status_code, 200)
        self.assertEquals(len(self.name), 10)
        self.assertIn(result_post.json()['name'], self.name)

    # create new board, get its details

    def test_get_details(self):
        result_post = post_board_name(self.name)
        board_id = result_post.json()['id']
        result_get = get_board_detals(board_id)

        self.assertEqual(result_get.status_code, 200)
        self.assertEquals(len(self.name), 10)
        self.assertIn(result_get.json()['id'], board_id)
        self.assertTrue('name', self.name)
        self.assertTrue(result_get.headers,'application/json')

    # change board's name, verify it's been changed

    def test_put_new_name(self):
        result_post = post_board_name(self.name)
        board_id = result_post.json()['id']
        result_put = put_new_board_name(board_id, board_name=self.name)

        self.assertEqual(result_put.status_code, 200)
        self.assertIn(result_put.json()['name'], self.name)


if __name__ == '__main__':
    unittest.main()