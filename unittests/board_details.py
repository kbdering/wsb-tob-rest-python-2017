import string
import random
import requests
from settings import *

''' Test Scenario: Create board with random name and get board's details.
Functionality checked: check basic Trello API functionality, i.e. creating a board, changing its name
'''

#create random name for our board

nonce = lambda x: ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10 if not x else x))

def get_board_detals(board_id):
    url = "https://trello.com/1/boards/" + board_id
    params = {"key":key,
          "token":token}
    return requests.get(url=url, params=params)

def post_board_name(board_name):
    url = "https://api.trello.com/1/boards"
    params = {"key": key,
          "token": token,
          "name": board_name}
    return requests.post(url=url, params=params)

def put_new_board_name(board_id, board_name):
    url = "https://api.trello.com/1/boards/" + board_id +"/name"
    params = {"key": key,
              "token": token,
              "value" : board_name}
    return requests.put(url=url, params=params)