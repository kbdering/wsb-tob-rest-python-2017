import string
import random
import requests
from settings import *

''' Test Scenario: Create card with random name and post it, get its id, then delete this card.
Make sure it has been deleted.
Functionality checked: check basic Trello API functionality, i.e. posting and deleting cards.

'''

#create random name for the card

nonce = lambda x: ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10 if not x else x))


def get_card(card_id):
    url = "https://trello.com/1/cards/" + card_id

    params = {"key":key,
          "token":token}

    return requests.get(url=url, params=params)

def post_card(card_name):
    url = "https://api.trello.com/1/cards"
    params = {"key": key,
          "token": token,
          "name": card_name,
          "idList": "590cf1a35ef46b9140e14227"}
    return requests.post(url=url, params=params)

def delete_card(card_id):
    url = "https://trello.com/1/cards/" + card_id

    params = {"key":key,
          "token":token}

    return requests.delete(url=url, params=params)
