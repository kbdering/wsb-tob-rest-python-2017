# README #

##The following repo contains test sets for Trello API.##
##For complete test coverage, the creators prepared 3 test approaches:##
* ###Unit testing, located in unittests path###
* ###GUI application, located in GUI path###
* ###Automated testing based on documentation - Automated Scenario Testing path###

##ABOUT Unit Tests:##
Unit testing was formed in standard testing convention, all the test scenarios
are executable from any IDE supporting the unittests library, like PyCharm.
The test files start with test_*.

### How do I get set up? ###

* Install Python: version 3.5 and above
* Open the test files with your IDE, or directly via python:

```
#!cmd
py test_board_operations.py
py test_board_details.py
py test_card_operations.py
```

##ABOUT GUI:##
The GUI application was made for the purpose of testing the JSON-based webservice (Trello API in particular) as a University project
for Software Testing. Its main functionalities include:
* sending HTTP requests(via Python requests library) 
* designing, saving, storing, loading and executing full test scenarios
* parsing JSON responses and extracting certain parameters
* using extracted parameters in subsequent test steps
* asserting request and response using a python script
* tracking the test step statuses

### How do I get set up? ###

* Install Python: version 3.5 and above
* Install requests and jsonpath_rw python modules
```
#!cmd
pip install requests
pip install jsonpath_rw
```
* Run GUI.bat file in ./GUI
* Open Sample test scenario included in this repository ( Menu > Test Plan > Open Test Plan )
* Execute the scenario ( Menu > Test Plan > Execute Test Plan)


##ABOUT Automated Scenario Testing:##
The automated scenario testing is a proof of concept to prove that testing of certain webservices can
 be automated based solely on documentation, if the documentation provides sufficient data (object models,
 response schemas, response types, validations).
 By now it covers:
 - Object creation with variation of availiable parameters
 - Object transition with recognition of transition types, fields, required objects and assertion
 of assertioned fields.
User doesn't have to plan the test scenarios. The concept is set to cover 100% of the possible operations
based on available params passed by user.
The test reports are generated in ./reports Path
### How do I get set up? ###

* Install Python: version 3.5 and above
* Install requests and xlslxwriter python modules
```
#!cmd
pip install requests
pip install xlslxwriter
```
run the automation.bat file
The test results will be visible in reports path


### REPO CREATORS : ###

* Anna Serafin
* Jakub Dering